#include <iostream>
#include <vector>
#include <ctime>
#include <algorithm>

using namespace std;

bool updateTopScores(vector<int> &in, vector<int> &out);
bool testUpdateTopScore();
bool compare(int i, int j);

int main() {

	//vector<int> topScores;
	//topScores = { 443, 439, 438, 438, 434, 418, 418, 417, 413, 411 };	//put values in the vector

	//int score = 444;

	//topScores.push_back(score);											//pushing 444 in the vector

	//vector <int> in;
	//vector <int> out;

	//in = { 1, 2, 3, 4, 5, 6 };
	//out = {};

	//if (updateTopScores(in, out)) {
	//	cout << "The score after the sort is.." << endl;

	//	for (int i = 0; i < 6; i++) {
	//		cout << out[i] << endl;
	//	}
	//}

	//comments added

	if (testUpdateTopScore()){
	cout << "\nEqual" << endl;
	}
	else
	cout << "\nNot equal";

	system("pause");
	return 0;
}

bool updateTopScores(vector<int> &in, vector<int> &out) {
	out = in;
	for (int i = 0; i < out.size(); i++) {
		for (int j = 0; j < out.size(); j++) {
			int temp = 0;
			if (out[i] > out[j]) {
				temp = out[i];			//now doing the swap of numbers
				out[i] = out[j];
				out[j] = temp;
			}
		}

	}

	return true;
}

bool testUpdateTopScore(){
	int arr[10];
	srand(time(NULL));
	int i = 0;
	for (int j = 0; j < 10; j++){
		i = (rand() % 100)+1;
		arr[j] = i;
	}

	cout << "\noriginal generated array is.." << endl;
	for (i = 0; i < 10; i++){
		cout << arr[i] << " , ";
	}

	
	vector <int> vec;
	for (int i = 0; i < 10; i++){
		//vec[i] = arr[i];
		vec.push_back(arr[i]);
	}


	sort(vec.begin(),vec.end(),compare);

	srand(time(0));
	int g = rand() % 565 + 1;
	cout << "\nThe number which is randomly generated and which will be pushed in the vector is" << g << endl;
	cout << "Now pushing back newly generated number in the vector and the vector becomes.."<< endl;
	vec.push_back(g);
	for (i = 0; i < vec.size(); i++){
		cout << vec[i] << " ";
	}

	vector <int> testVect;

	cout << "\nNow sorting the vector using updateTopScore function.." << endl;

	updateTopScores(vec, testVect);
	for (i = 0; i < testVect.size(); i++){
		cout << testVect[i] << " ";
	}

	cout << "\nNow sorting the vector using sort function of c++" << endl;

	sort(vec.begin(), vec.end(), compare);
	for (i = 0; i < 11; i++){
		cout << vec[i] << " ";
	}

	return vec == testVect;

}

bool compare(int i, int j){
	return i>j;
}